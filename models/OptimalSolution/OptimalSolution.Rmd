---
title: "Optimal Solution"
author: "Kira"
date: "3/29/2021"
output:
  pdf_document: default
  html_document: default
---

```{r}
if (!require(plyr)) {install.packages("plyr"); library(plyr)}
if (!require(dplyr)) {install.packages("dplyr"); library(dplyr)}
if (!require(lme4)) {install.packages("lme4"); library(lme4)}
source('../utils.R')
```

```{r}
# Load dataframe, depending on the enviroment use different path
try(df_solution <- read.csv("../../data/df_solutions_v2.csv"))
try(df_solution <- read.csv("data/df_solutions_v2.csv"))


# Set factors
df_solution$hybridChain <- as.factor(df_solution$isChainWithMachine)
df_solution$envIdx <- as.factor(df_solution$envIdx)
df_solution$actorIdx <- as.factor(as.character(df_solution$actorIdx))
df_solution$chainIdx <- as.factor(df_solution$chainIdx)
df_solution$generation <- as.factor(df_solution$Generation)
df_solution$class <- as.factor(df_solution$class)


# We use round as a numeric variable
df_solution$round <- as.numeric(df_solution$round)
# We scale round from 0 to 1
df_solution$round <- df_solution$round / 80

# Rename factors
df_solution$class  <- revalue(
    df_solution$class, c("Human Regretfull"="HumanRegretful", "Riskseeking Regretfull"="HumanRewarding"))

df_solution$class <- relevel(df_solution$class, ref = "HumanRegretful")

# Filter out random first solutions and machine solution
data <- df_solution %>%
  filter(isMachineSolution=='False')
```

```{r}
for(i in 1:length(data$totalRegret)) {
  if (data$totalRegret[i]==0) {
   data$optimalSolution[i] <- 1
  }
  else{
    data$optimalSolution[i] <- 0
  }
}


```
```{r}

# --- machine descendants ---
#  Generation since Machine Exposure
new1 <- c(rep(NA,length(data$generation)))
new2 <- c(rep(NA,length(data$generation)))
new3 <- c(rep(NA,length(data$generation)))

for(i in 1:length(data$generation)){
  if(data$generation[i]==1){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==2){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"m"
      new2[i]<-"m"
      new3[i]<-"m"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==3){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"1"
      new2[i]<-"1"
      new3[i]<-"1"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==4){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"2"
      new2[i]<-"2"
      new3[i]<-"2"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==5){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"3"
      new2[i]<-">2"
      new3[i]<-"3"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==6){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"4"
      new2[i]<-">2"
      new3[i]<-">3"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==7){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"5"
      new2[i]<-">2"
      new3[i]<-">3"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
  if(data$generation[i]==8){
    if(data$hybridChain[i]=="True"){
      new1[i]<-"6"
      new2[i]<-">2"
      new3[i]<-">3"
    }
    else{
      new1[i]<-"no"
      new2[i]<-"no"
      new3[i]<-"no"
    }
  }
}



data$machineDescendant2<-new2

data$machineDescendant2 <- as.factor(data$machineDescendant2)

data$machineDescendant2 <- relevel(data$machineDescendant2, ref = "no")

# --- generation ---
# Numeric variable: 0 to 7 for generation 1 to 8
data$generation <- as.numeric(data$generation)-1
```





```{r}
data$machineDescendant <- data$machineDescendant2
data$round <- scale(data$round)
data$generation <- scale(data$generation)

m <- glmer(optimalSolution ~ class * machineDescendant + class * generation + round + (1|actorIdx) + (1|envIdx) , family = "binomial", data=data, control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=2e5)))
summary(m)
```


```{r}
conf.level = .95
statsname = 'Z'

cm <- c(
  "classHumanRewarding" = "HumanRewEnv",
  "machineDescendant1" = "G_{Hy3}",
  "machineDescendant2" = "G_{Hy4}",
  "machineDescendant3" = "G_{Hy5}",
  "machineDescendant4" = "G_{Hy6}",
  "machineDescendant5" = "G_{Hy7}",
  "machineDescendant6" = "G_{Hy8}",
  "machineDescendant>2" = "G_{Hy5+}",
  "machineDescendant>3" = "G_{Hy6+}",
  "envIdx" = "environment",
  "actorIdx" = "participant",
  "Residual" = "residual"
)

create_csv(m, "results/optimal.csv", coeff_map = coeff_map, conf.level = conf.level, statsname=statsname)
```



