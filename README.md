# Reward Networks GLMM

## Reward of Solutions

`models/RewardModel/Reward_Model.Rmd`

Linear mixed model predicting the reward of a solution.

R script to create Supplementary Table 1.

## Copying of solution

`models/CopySolution/CopySolutionGLMER.Rmd`

Generalized mixed model predicting number of copied actions.

R script to create table Supplementary Table 2 and 3.

## OptimalSolution

`models/OptimalSolution/OptimalSolution.Rmd`

Generalized mixed model predicting whether a solution is optimal.
