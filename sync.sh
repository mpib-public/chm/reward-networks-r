#!/bin/bash

set -e

OVERLEAF_PROJECT=5ecd1e28afb61d0001e6ec52

# git config credential.helper '!f() { sleep 1; echo "username=${OVERLEAF_USER}"; echo "password=${OVERLEAF_PW}"; }; f'

git config --global user.email ${OVERLEAF_USER}

git clone https://${OVERLEAF_USER}:${OVERLEAF_PW}@git.overleaf.com/${OVERLEAF_PROJECT}

rm -rf ${OVERLEAF_PROJECT}/R
mkdir -p ${OVERLEAF_PROJECT}/R
cp -r models ${OVERLEAF_PROJECT}/R/.
cd ${OVERLEAF_PROJECT}
git add .
git commit -m 'Update.'
git push
cd ..
rm -rf ${OVERLEAF_PROJECT}